var mongoose = require('mongoose');

/**
 * Example schema
 */

const userSchema = new mongoose.Schema({
    email: { type: String },
    password : {type : String},
    created : { type : Date},
});

/**
 * Events
 */

// on every save, add the dates
userSchema.pre('save', function(next) {
    this.created = Date.now;
    next();
});

/**
 * Statics
 */

/**
 * Find a user.
 *
 * @param {String} email
 * @param {Function} callback
 * @api public
 * @static
 */
userSchema.statics.findByEmail = function(email, callback) {
    return this.findOne({'email' :  email}).exec(callback);
};

/**
 * Match password.
 * 
 * @param {String} password
 * @api public
 * @method
 */

userSchema.methods.matchPassword = function (password){
    return this.password == password;
}

/**
 * Exports
 */
module.exports = mongoose.model('userModel', userSchema);
