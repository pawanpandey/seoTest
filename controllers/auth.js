/**
 * Login  Controller.
 */
const jwt = require('../helpers/jwt');
const users = require('../models/UserModel');
module.exports = {
    userAuth: function(req, res) {
        const email = req.body.email;
        const password = req.body.password;

        if (!email || !password) {
            return res.status(401).send({
                err: 'email and password required'
            });
        }

        users.findByEmail(email)
        .then(function(user){
           
            if(!user){
                return res.status(401).send({
                    err: 'invalid email and password'
                }); 
            }else if(user.matchPassword(password)){
                const token =   jwt.issue({
                                    sub : user._id,
                                });
                
                 return res.status(200).send({
                     user : user,
                     token : token
                 });
            }else{
                  return res.status(401).send({
                    err: 'invalid email and password'
                }); 
            }
        }).catch(function(err){
            return res.status(500).send(err);
        })
    }

};