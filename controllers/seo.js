var SeoModel = require('../models/SeoModel');



function createSeo(body, callback) {
  SeoModel.findOneAndUpdate({ 'cus_id' : body.cus_id }, { '$set' : body } , { upsert : true, new : true } ,function (err, seo) {
      if (err) {
          return callback(err);
      }

      callback(null, seo);
  } );

}

function findSeo(callback) {

    SeoModel.find(callback);
}

findArticle = function(callback){
    SeoModel.find({"type":"article"},callback);
}

findUser = function(callback){
    SeoModel.find({"type":"user"},callback);
}

findTag = function(callback){
    SeoModel.find({"type":"tag"},callback);
}
findIndustry = function(callback){
    SeoModel.find({"type":"industry"},callback);
}

findSeoById = function(id,callback){
    SeoModel.find({"_id":id},callback);
}

findArticleById = function(id,callback){
    SeoModel.find({"cus_id":id,"type":"article"},callback);
}

findUserById = function(id,callback){
    SeoModel.find({"cus_id":id,"type":"user"},callback);
}

findTagById = function(id,callback){
    SeoModel.find({"cus_id":id,"type":"tag"},callback);
}
findIndustryById = function(id,callback){
    SeoModel.find({"cus_id":id,"type":"industry"},callback);
}
////////////////////////////////////////////

var self = {
    findAll: findSeo,
    create: createSeo,
    findArticle: findArticle,
    findUser: findUser,
    findTag:findTag,
    findIndustry : findIndustry,
    findSeoById: findSeoById,
    findArticleById:findArticleById,
    findUserById:findUserById,
    findTagById:findTagById,
    findIndustryById:findIndustryById
};

module.exports = self;
