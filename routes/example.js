

var express = require('express');
var router = express.Router();

var ExampleHandler = require('../lib/exampleHandler');

router.get('/', function(req, res) {

    ExampleHandler.findAll(function(err, users) {
        if (err) {
            return res.status(400).json({error: err});
        }

        return res.json(users);
    });

});

router.post('/', function(req, res) {
    ExampleHandler.createUser(req.body, function(err, result) {
        if (err) {
            return res.status(400).json({error: err});
        }

        return res.json(result);
    });
});

module.exports = router;
