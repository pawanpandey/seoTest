const jwt = require('../helpers/jwt');

module.exports = function (req, res, next) {
  var token;

  if (req.headers && req.headers.authorization) {
    var parts = req.headers.authorization.split(' ');
    if (parts.length == 2) {
      var scheme = parts[0],
        credentials = parts[1];
        req.token = parts[1]; // token for next middleware.

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else {
      return res.json(401, {err: 'Format is Authorization: Bearer [token]'});
    }
  }  else if (req.param('token')) {
    token = req.param('token');
    req.token = token;
    // We delete the token from param to not mess with blueprints
    delete req.query.token;
  }
  else if (req.socket && req.socket.handshake && req.socket.handshake.query && req.socket.handshake.query.token) {
        token = req.socket.handshake.query.token;
        req.token = req.socket.handshake.query.token;
      }
  else {
    return res.json(401, {err: 'No Authorization header was found'});
  }





  jwt.verify(token, function (err, tokenData) {
    if (err) return res.json(401, {err: 'Invalid Token!'});
    req.tokenData = tokenData; // This is the decrypted token or the payload you provided
    next();
  });
};