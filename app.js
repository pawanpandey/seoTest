var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var errorHandler = require('errorhandler');
var config = require('config');
var routes = require('./routes');
var expressLogger = require('./lib/meta-logger')('YourAppNameHere').expressLogger;

var app = express();

var httpPort = config.get('port');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.set('port', httpPort);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(errorHandler());
app.use(cors());

app.use('/css',express.static(path.join(__dirname + '/doc/css')));
app.use('/img',express.static(path.join(__dirname + '/doc/img')));
app.use('/locales',express.static(path.join(__dirname + '/doc/locales')));
app.use('/utils',express.static(path.join(__dirname + '/doc/utils')));
app.use('/vendor',express.static(path.join(__dirname + '/doc/vendor')));
app.use('/api_data.js',express.static(path.join(__dirname + '/doc/api_data.js')));
app.use('/api_data.js',express.static(path.join(__dirname + '/doc/api_data.json')));
app.use('/api_project.js',express.static(path.join(__dirname + '/doc/api_project.js')));
app.use('/api_project.json',express.static(path.join(__dirname + '/doc/api_project.json')));
app.use('/main.js',express.static(path.join(__dirname + '/doc/main.js')));
app.get('/', function(req, res) {
  var username = req.query.username, password = req.query.password;
  if(username == "pawan@fourzip.com" && password == "testpassword"){
    res.sendFile(path.join(__dirname+'/doc/index.html'));
  }else{
    res.status(401).send({
      data:[],
      errors:["unauthorized access"],
      errorFor:{}
    });
  }
});


// setup logging
if (process.env.NODE_ENV !== 'test') {
    app.use(expressLogger);
}

// setup router
app.use('/', routes);

module.exports = app;
